//Vladyslav Berezhnyak (1934443)

public class Bicycle 
{
	private String manufacturer;
	private int numberGears;
	private double maxSpeed;
	
	public Bicycle(String man, int numG, double mSpd)
	{
		this.manufacturer = man;
		this.numberGears = numG;
		this.maxSpeed = mSpd;
	}
	
	public String getManufacturer()
	{
		return this.manufacturer;
	}
	
	public int getNumberGears()
	{
		return this.numberGears;
	}
	
	public double getMaxSpeed()
	{
		return this.maxSpeed;
	}
	
	public String toString()
	{
		StringBuilder s = new StringBuilder();
		
		s.append("Manufacturer: " + this.manufacturer + "\n" +
				 "Number of Gears: " + this.numberGears + "\n" +
				 "Max Speed: " + this.maxSpeed + " Km/h \n");
		
		return s.toString();
	}
}
