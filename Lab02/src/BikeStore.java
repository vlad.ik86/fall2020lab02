//Vladyslav Berezhnyak (1934443)

public class BikeStore 
{
	public static void main(String[] args)
	{
		//Initialize Bicycle[]
		Bicycle[] inventory = new Bicycle[4];
		inventory[0] = new Bicycle("Paramount", 5, 30.0);
		inventory[1] = new Bicycle("Yamamouto", 4, 35.0);
		inventory[2] = new Bicycle("Mystic", 6, 28.0);
		inventory[3] = new Bicycle("Magnum", 3, 40.0);
		
		for(int i = 0; i < inventory.length; i++)
		{
			System.out.println("Bicycle #" + (i + 1) + "\n" + inventory[i]);
		}
	}
}
